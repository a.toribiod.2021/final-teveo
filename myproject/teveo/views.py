import os
import random

import requests
from django.conf import settings
from django.db.models import Count
from django.shortcuts import render, get_object_or_404, redirect
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.template import loader

from .models import Camara, Comentario, Session_db
from xml.etree import ElementTree


def update_sesiondb(request):
    """
    Actualiza la sesión de la base de datos con los datos de la sesión actual
    """
    Session_db.objects.update_or_create(
        defaults={'user_name': request.session['user_name'],
                  'font_type': request.session['font_type'],
                  'font_size': request.session['font_size']},
        id=request.session.session_key
    )


def manage_id_sesion(request):
    """
    Mantiene la sesión de la base de datos actualizada con los datos de la sesión actual
    """
    if not request.session.session_key:
        request.session['user_name'] = 'Anónimo'
        request.session['font_size'] = '14'
        request.session['font_type'] = 'Arial'
    else:
        update_sesiondb(request)


def download_xml_data(url):
    """
    Comprueba si el archivo XML existe en la ruta especificada y devuelve la ruta del archivo
    """
    if not os.path.exists(url):
        print(f"Error: El archivo XML no se encuentra en la ruta especificada: {url}")
        return
    else:
        return url


def parse_xml_data(xml_data, path):
    """
    Parsea el archivo XML y devuelve una lista de diccionarios con los datos de las cámaras
    """
    tree = ElementTree.parse(xml_data)
    cameras = []
    # Determine the XML structure and parse accordingly
    if path == "teveo/xml/listado1.xml":
        root = tree.getroot()
        for item in root.findall('camara'):
            coordinates = item.find('coordenadas').text.split(',')
            cameras.append({
                'id': item.find('id').text,
                'src': item.find('src').text,
                'lugar': item.find('lugar').text,
                'latitude': float(coordinates[1].strip()),
                'longitude': float(coordinates[0].strip())
            })
    elif path == "teveo/xml/listado2.xml":
        root = tree.getroot()
        for item in root.findall('cam'):
            cameras.append({
                'id': item.get('id'),
                'src': item.find('url').text,
                'lugar': item.find('info').text,
                'latitude': float(item.find('.//latitude').text),
                'longitude': float(item.find('.//longitude').text)
            })
    else:
        return HttpResponse("Not valid source", status=500)

    return cameras


def cameras_json(request, identifier):
    """
    Devuelve los datos de una cámara en formato JSON
    """
    camera = get_object_or_404(Camara, id=identifier)
    comments_count = Comentario.objects.filter(camara=camera).count()
    camera_data = {
        'id': camera.id,
        'name': camera.name,
        'latitude': camera.latitude,
        'longitude': camera.longitude,
        'source': camera.source,
        'url': camera.url,
        'comments_count': comments_count
    }
    return JsonResponse(camera_data)


def download_image(url, filename):
    """
    Descarga una imagen desde una URL y la guarda en la ruta especificada
    """
    response = requests.get(url)
    if response.status_code == 200:
        file_path = os.path.join(settings.MEDIA_ROOT, 'cameras', filename)
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        with open(file_path, 'wb') as file:
            file.write(response.content)
        return file_path
    return None


def import_cameras_from_url(request, list_id):
    """
    Importa las cámaras desde una ruta y las guarda en la base de datos
    """
    if list_id == "1":
        url = "teveo/xml/listado1.xml"
        source_prefix = "LIS1"
    elif list_id == "2":
        url = "teveo/xml/listado2.xml"
        source_prefix = "LIS2"
    else:
        return HttpResponse("Not valid source", status=500)

    xml_data = download_xml_data(url)
    if xml_data:
        cameras = parse_xml_data(xml_data, url)
        for cam in cameras:
            identifier = f"{source_prefix}-{cam['id']}"
            Camara.objects.get_or_create(
                id=identifier,
                defaults={'name': cam['lugar'],
                          'latitude': cam['latitude'],
                          'longitude': cam['longitude'],
                          'source': source_prefix,
                          'url': cam['src']}
            )

    return redirect('cameras_list')


def index(request):
    """
    Muestra la página principal de la aplicación
    """
    manage_id_sesion(request)
    comments = Comentario.objects.all().order_by('-fecha')
    cameras_count = Camara.objects.count()
    comments_count = Comentario.objects.count()
    if request.GET.get('id'):
        try:
            session = Session_db.objects.filter(
                id=request.GET.get('id')).first()
            request.session['user_name'] = session.user_name
            request.session['font_type'] = session.font_type
            request.session['font_size'] = session.font_size
        except BaseException:
            pass
    return render(request, 'index.html', {
        'user_name': request.session['user_name'],
        'comments': comments,
        'cameras_count': cameras_count,
        'comments_count': comments_count,
    })


def cameras_list(request):
    """
    Muestra la lista de cámaras con los datos de cada una
    """
    manage_id_sesion(request)
    cameras = Camara.objects.annotate(
        comments_count=Count('comentario')).order_by('-comments_count')
    cameras_count = cameras.count()
    comments_count = Comentario.objects.count()
    random_camera = Camara.objects.order_by('?').first()
    return render(request, 'cameras_list.html', {
        'user_name': request.session['user_name'],
        'cameras': cameras,
        'random_camera': random_camera,
        'cameras_count': cameras_count,
        'comments_count': comments_count,
    })


def camera_detail(request, identifier):
    """
    Muestra los detalles de una cámara
    """
    manage_id_sesion(request)
    camera = get_object_or_404(Camara, id=identifier)
    comments = Comentario.objects.filter(camara=camera).order_by('-fecha')
    cameras_count = Camara.objects.count()
    comments_count = Comentario.objects.count()
    return render(request, 'camera_detail.html', {
        'user_name': request.session['user_name'],
        'camera': camera,
        'comments': comments,
        'cameras_count': cameras_count,
        'comments_count': comments_count,
    })


def camera_dynamic(request, identifier):
    """
    Muestra los detalles de una cámara dinámica que se recarga cada 30 segundos
    """
    manage_id_sesion(request)
    camera = get_object_or_404(Camara, id=identifier)
    comments = Comentario.objects.filter(camara=camera).order_by('-fecha')
    cameras_count = Camara.objects.count()
    comments_count = Comentario.objects.count()
    return render(request, 'camera_dynamic.html', {
        'camera': camera,
        'comments': comments,
        'cameras_count': cameras_count,
        'comments_count': comments_count,
    })


def add_comment(request):
    """
    Agrega un comentario a una cámara
    """
    manage_id_sesion(request)
    if request.method == 'POST':
        camera_id = request.POST.get('camara_id')
        text = request.POST.get('text')
        autor = request.session['user_name']

        if not camera_id or not text:
            return HttpResponseBadRequest("Missing camera_id or text")

        camera = get_object_or_404(Camara, id=camera_id)
        id_comentario = random.randint(10000, 99999)
        comentario = Comentario.objects.create(
            camara=camera,
            texto=text,
            url=camera.url,
            autor=autor,
            id=id_comentario)
        image_filename = f"{comentario.id}.jpg"
        image_path = os.path.join('cameras/', image_filename)

        if not os.path.exists(image_path):
            image_data = download_image(camera.url, image_filename)
            if image_data is None:
                return HttpResponse("Image not found", status=500)

        return redirect('camera_detail', identifier=camera_id)

    elif request.method == 'GET':
        camera_id = request.GET.get('camera_id')
        camera = get_object_or_404(Camara, id=camera_id)
        return render(request, 'add_comment.html', {
            'camera': camera,
            'user_name': request.session['user_name'],
        })
    else:
        return HttpResponseBadRequest("Invalid request method")


def configuration(request):
    """
    Muestra la página de configuración de la aplicación
    """
    manage_id_sesion(request)
    if request.method == "POST":
        if 'font_size' in request.POST:
            request.session['font_size'] = request.POST['font_size']
        if 'font_type' in request.POST:
            request.session['font_type'] = request.POST['font_type']
        if ((('user_name' in request.POST) and
             Session_db.objects.filter(user_name=request.POST['user_name']).exists())
                and not request.POST['user_name'] == request.session['user_name']):
            pass
        elif 'user_name' in request.POST:
            request.session['user_name'] = request.POST['user_name']
        update_sesiondb(request)

    cameras_count = Camara.objects.count()
    comments_count = Comentario.objects.count()

    # Renderizar la plantilla con los datos actuales de configuración
    return render(request, 'configuration.html', {
        'user_name': request.session['user_name'],
        'cameras_count': cameras_count,
        'comments_count': comments_count,
        'font_size': request.session['font_size'],
        'font_type': request.session['font_type'],
        'id': request.session.session_key,
    }
    )


def help_page(request):
    """
    Muestra la página de ayuda de la aplicación
    """
    manage_id_sesion(request)
    cameras_count = Camara.objects.count()
    comments_count = Comentario.objects.count()
    return render(request, 'help.html', {
        'user_name': request.session['user_name'],
        'cameras_count': cameras_count,
        'comments_count': comments_count,
    })


def image_dyn(request, identifier):
    """
    Devuelve la imagen de una cámara dinámica que se recarga cada 30 segundos
    """
    # Buscar la cámara por el identificador
    camera = get_object_or_404(Camara, id=identifier)
    # Construir la respuesta HTML con la imagen
    response_html = f'<img src="{
        camera.url}" alt="Imagen de la cámara">'
    return HttpResponse(response_html)


def get_css(request):
    """
    Devuelve el CSS de la aplicación
    """
    font_type = request.session['font_type']
    font_size = request.session['font_size']

    context = {
        'font_type': font_type,
        'font_size': font_size,
    }

    template = loader.get_template('css/style.css')
    css = template.render(context)
    return HttpResponse(css, content_type='text/css')
