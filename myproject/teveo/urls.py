from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('cameras/', views.cameras_list, name='cameras_list'),
    path('cameras/<str:identifier>.json/', views.cameras_json, name='cameras_json'),
    path('cameras/<str:identifier>-dyn/', views.camera_dynamic, name='camera_dynamic'),
    path('cameras/<str:identifier>/', views.camera_detail, name='camera_detail'),
    path('comentario/', views.add_comment, name='add_comment'),
    path('config/', views.configuration, name='configuration'),
    path('help/', views.help_page, name='help_page'),
    path('download/<str:list_id>/', views.import_cameras_from_url, name='import_cameras_from_url'),
    path('image_dyn/<str:identifier>/', views.image_dyn, name='image_dyn'),
    path('style.css', views.get_css, name='get_css'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
