# tests.py

import random
from django.test import TestCase, Client
from django.urls import reverse
from .models import Camara, Comentario


class TeVeOTests(TestCase):

    def setUp(self):
        self.client = Client()
        self.session_key = self.client.session.session_key

        # Crear una cámara de prueba
        self.camera = Camara.objects.create(
            id="test-cam-1",
            name="Test Camera",
            latitude=40.416775,
            longitude=-3.703790,
            source="TEST",
            url="http://example.com/test.jpg"
        )

        # Crear un comentario de prueba
        self.comment = Comentario.objects.create(
            camara=self.camera,
            texto="This is a test comment.",
            url=self.camera.url,
            autor="Test Author",
            id=random.randint(10000, 99999)
        )

        # Inicializar sesión
        session = self.client.session
        session['user_name'] = 'Test User'
        session['font_size'] = '14'
        session['font_type'] = 'Arial'
        session.save()

    def test_index_view(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_cameras_list_view(self):
        response = self.client.get(reverse('cameras_list'))
        self.assertEqual(response.status_code, 200)

    def test_camera_detail_view(self):
        response = self.client.get(reverse('camera_detail', args=['test-cam-1']))
        self.assertEqual(response.status_code, 200)

    def test_camera_dynamic_view(self):
        response = self.client.get(reverse('camera_dynamic', args=['test-cam-1']))
        self.assertEqual(response.status_code, 200)

    def test_configuration_view_get(self):
        response = self.client.get(reverse('configuration'))
        self.assertEqual(response.status_code, 200)

    def test_configuration_view_post(self):
        response = self.client.post(reverse('configuration'), {
            'font_size': '16',
            'font_type': 'Calibri',
            'user_name': 'Updated User'
        })
        self.assertEqual(response.status_code, 200)
        session = self.client.session
        self.assertEqual(session['font_size'], '16')
        self.assertEqual(session['font_type'], 'Calibri')
        self.assertEqual(session['user_name'], 'Updated User')

    def test_help_page_view(self):
        response = self.client.get(reverse('help_page'))
        self.assertEqual(response.status_code, 200)

    def test_get_css_view(self):
        response = self.client.get(reverse('get_css'))
        self.assertEqual(response.status_code, 200)

    def test_import_cameras_from_url(self):
        response = self.client.get(reverse('import_cameras_from_url', args=['1']))
        self.assertEqual(response.status_code, 302)  # Redirecciona después de importar

