from django.contrib import admin
from .models import Camara, Comentario

admin.site.register(Camara)
admin.site.register(Comentario)
