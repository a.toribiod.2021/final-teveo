# Generated by Django 5.0.3 on 2024-06-04 16:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teveo', '0003_camara_remove_comment_camera_comentario_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Session_db',
            fields=[
                ('id', models.CharField(max_length=32, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100, null=True)),
                ('letter_type', models.CharField(max_length=100, null=True)),
                ('letter_size', models.CharField(max_length=100, null=True)),
                ('theme_type', models.CharField(max_length=100, null=True)),
            ],
        ),
    ]
