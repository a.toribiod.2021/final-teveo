from django.db import models


class Camara(models.Model):
    id = models.CharField(max_length=100, unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    latitude = models.FloatField()
    longitude = models.FloatField()
    source = models.CharField(max_length=50)
    url = models.URLField(default='')

    def __str__(self):
        return self.id


class Comentario(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    texto = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)
    url = models.URLField()
    autor = models.CharField(max_length=100)
    id = models.IntegerField(primary_key=True)

    def __str__(self):
        return f'Comentario para {self.camara} a {self.fecha}'


class Session_db(models.Model):
    id = models.CharField(max_length=32, primary_key=True)
    user_name = models.CharField(max_length=100, null=True)
    font_type = models.CharField(max_length=100, null=True)
    font_size = models.CharField(max_length=100, null=True)

